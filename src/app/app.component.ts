import { Component } from "@angular/core";
import { Product } from "./models/product";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: [],
})
export class AppComponent {
  title = "tp1";
  products: Product[] = [
    {
      name: "Canapé jaune",
      cover: "assets/canape-jaune.png",
      details: "Livraison sous 2 semaines",
      price: 799,
    },
    {
      name: "Chaise bois",
      cover: "assets/chaise-bois.png",
      details: "Livraison sous 3 jours",
      price: 299,
      discountedPrice: 99,
    },
    {
      name: "Fauteuil jaune",
      cover: "assets/fauteuil-jaune.png",
      details: "Rupture de stock",
    },
  ];
}
