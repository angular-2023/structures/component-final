import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import localeFr from "@angular/common/locales/fr";
import { registerLocaleData } from "@angular/common";

import { AppComponent } from "./app.component";
import { ProductItemComponent } from "./component/product-item/product-item.component";

registerLocaleData(localeFr, "fr-FR");
@NgModule({
  declarations: [AppComponent, ProductItemComponent],
  imports: [BrowserModule],
  bootstrap: [AppComponent],
  providers: [
    { provide: LOCALE_ID, useValue: "fr-FR" },
    { provide: DEFAULT_CURRENCY_CODE, useValue: "EUR" },
  ],
})
export class AppModule {}
